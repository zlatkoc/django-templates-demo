from django.shortcuts import render


def index(request):
    return render(request, 'frontend/index.html')


def page1(request):
    return render(request, 'frontend/page1.html')


def page2(request):
    return render(request, 'frontend/page2.html')


def page3(request):
    return render(request, 'frontend/page3.html')
